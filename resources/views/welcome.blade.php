@extends('layouts.app')

@section('content')
    <h2>Ejemplos datatables</h2>

    <div class="well">
        <table class="table" id="users-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
            </tr>
            </thead>
        </table>
    </div>

    <div class="well">
        <table class="table" id="products-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>

        $.extend(true, $.fn.dataTable.defaults, {
            info: true,
            paging: true,
            ordering: true,
            searching: true,
            language: {
              url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
        });

        $("#users-table").DataTable({
            // "iDisplayLength": 100,
            paging: false,
            stateSave: false,
            scrollY: "40vh",
            columns: [
                {name: "id", data: "id"},
                {name: "name", data: "name"},
                // {name: "email", data: "email", visible: false, searchable: false},
                {name: "email", data: "email", render: function (data, type, row) {
                        return `(${row.id}) ${data}`;
                    },
                },
            ],
            columnsDefs: [
                { targets: [0], orderData: [0, 1]},
                { targets: [1], orderData: [1, 2]},
                { targets: 2 },
            ],
            ajax: {
                url: "http://datatables.test/data/users.json"
            }
        });

        $("#products-table").DataTable({
            columns: [
                {name: "id", data: "id"},
                {name: "name", data: "name"},
                {name: "price", data: "price"},
            ],
            columnsDefs: [
                { targets: [0], orderData: [0, 1]},
                { targets: [1], orderData: [1, 2]},
                { targets: [2], orderData: [2, 1]},
            ],
            ajax: {
                url: "http://datatables.test/data/products.json"
            }
        });
    </script>
@endsection
